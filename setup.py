#!/usr/bin/env python3

import setuptools

if __name__ == "__main__":
    setuptools.setup(
        name="zms-propsim-tirem",
        version="0.1.0",
        author="Serhat Tadik, Aashish Gottipati, Michael A. Varner, David M. Johnson, Gregory D. Durgin",
        author_email="serhat.tadik@gatech.edu",
        maintainer="David M. Johnson",
        maintainer_email="johnsond@flux.utah.edu"
        url="https://gitlab.flux.utah.edu/openzms/zms-propsim-tirem",
        description="A TIREM-based propagation service for OpenZMS.",
        classifiers=[
            "Development Status :: 3 - Alpha",
            "Environment :: Other Environment",
            "Intended Audience :: Developers",
            "Operating System :: OS Independent",
            "Programming Language :: Python",
            "Programming Language :: Python :: 3",
            "Topic :: Utilities",
        ],
        packages=setuptools.find_packages(),
        install_requires=[
            "zms-api @ git+https://gitlab.flux.utah.edu/openzms/zms-api.git@029acc8#subdirectory=python",
        ],
        python_requires=">=3.7",
        setup_requires=[
            "setuptools",
        ],
    )
