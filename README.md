# zms-propsim-tirem (a TIREM- and python-based propagation simulation tool for OpenZMS)

The [OpenZMS software](https://gitlab.flux.utah.edu/openzms) is a prototype
automatic spectrum-sharing management system for radio dynamic zones.
OpenZMS provides mechanisms to share electromagnetic (radio-frequency)
spectrum between experimental or test systems and existing spectrum users,
and between multiple experimental systems.  We are building and deploying
OpenZMS within the context of the POWDER wireless testbed in Salt Lake City,
Utah, part of the NSF-sponsored Platforms for Advanced Wireless Research
program, to create [POWDER-RDZ](https://rdz.powderwireless.net).

This repository contains a TIREM- and python-based propagation simulation
tool, and a gRPC service that implements the OpenZMS `propsim` API.  You
must provide your own TIREM binary when building the Dockerfile.
