
class PropSimTiremError(Exception):
    pass

class ConfigError(PropSimTiremError):
    pass

class JobExistsError(PropSimTiremError):
    pass

class InvalidJobError(PropSimTiremError):
    pass

class InvalidJobOutputError(PropSimTiremError):
    pass

class JobParameterError(PropSimTiremError):
    pass
