import os

MAP_TYPE = os.getenv("MAP_TYPE", "fusion")
MAP_FILENAME = os.getenv("MAP_FILENAME", "SLCmap_5May2022.mat")
GAIN = float(os.getenv("GAIN") or 0.0)
TXPOWER = float(os.getenv("TXPOWER") or 0.0)
SIDE_LEN = float(os.getenv("SIDE_LEN", 0.0))
SAMPLING_INTERVAL = float(os.getenv("SAMPLING_INTERVAL") or 1.0)
PARALLELIZE = int(os.getenv("PARALLELIZE", 0))
REFRAC = float(os.getenv("REFRAC", 450))
CONDUC = float(os.getenv("CONDUC", 50))
PERMIT = float(os.getenv("PERMIT", 81))
HUMID = float(os.getenv("HUMID", 80))

class TiremParams:
    """Represents TIREM Input Parameters"""

    def __init__(self, radio_name, radio_is_tx, txheight, rxheight, radio_lon,
                 radio_lat, freq, polarz,
                 map_type=MAP_TYPE, map_filename=MAP_FILENAME,
                 gain=GAIN, txpower=TXPOWER,
                 refrac=REFRAC, conduc=CONDUC, permit=PERMIT, humid=HUMID,
                 side_len=SIDE_LEN, sampling_interval=SAMPLING_INTERVAL,
                 extsn=False, parallelize=PARALLELIZE, generate_features=False, shrink_factor=1):
        """
        :param radio_name: basestation endpoint name e.g. "cbrssdr1-ustar-comp"
        :param radio_is_tx: Is the basestation a transmitter (1) or a receiver (0)
        :param txheight: transmitter height (m) (do not include the building height or elevation, just the height above the surface it rests upon)
        :param rxheight: receiver height (m) (do not include the building height or elevation, just the height above the surface it rests upon)
        :param radio_lon: longitude of the base station
        :param radio_lat: latitude of the base station
        :param freq: transmit frequency in MHz
        :param polarz: polarization of the wave produced by the tx antenna ('H' = horizontal polarization of Tx antenna and 'V' = vertical polarization)
        :param parallelize: the number of parallel TIREM processes to spawn and run; set to True or -1 to parallelize amongst multiprocessing.cpu_count() subtasks (or max of 61 on Windows); set to a specific integer number of subtasks; or set to False or 0 to disable parallel, multi-process execution.
        :param generate_features: generate only TIREM predictions (0) / also generate features (1)
        :param map_type: Fusion map ("fusion") or lidar DSM map ("lidar"). The fusion map struct has to have the fields "dem" for digital elevation map and "hybrid_bldg" for building heights. The lidar map has to have the field "data" having combined information of elevations and building heights.
        :param map_filename: map filename
        :param gain: antenna gain in dB
        :param txpower: transmitter power (dBm)
        :param extsn: boolean, 0 is false; anything else is true. False = new profile, true = extension of last profile
        :param refrac: surface refractivity, range: (200 to 450.0) "N-Units"
        :param conduc: conductivity, range: (0.00001 to 100.0) S/m
        :param permit: relative permittivity of earth surface, range: (1 to 1000)
        :param humid: humidity, units g/m^3, range: (0 to 110.0)
        :param side_len: grid cell side length (m).  If 0, will be determined by the cellsize data field in the map structure.
        :param sampling_interval: sampling interval along the tx-rx link. e.g. 0.5 means a given grid cell is sampled twice. Side length of 0.5 and sampling interval of 0.5 mean that the Tx-Rx horizontal array step size 0.5*0.5 = 0.25 m.
        :param shrink_factor: Compute only the first shrink_factor columns and shrink_factor rows.  If shrink_factor is 20, only computes 1/400 of the job.  Useful only for debugging.
        """

        self.radio_name = radio_name
        self.radio_is_tx = radio_is_tx
        self.txheight = txheight
        self.rxheight = rxheight
        self.radio_lon = radio_lon
        self.radio_lat = radio_lat
        self.freq = freq
        self.polarz = polarz
        self.radio_x = None
        self.radio_y = None
        self.parallelize = parallelize
        self.generate_features = generate_features
        self.map_type = map_type
        self.map_filename = map_filename
        self.gain = gain
        self.txpower = txpower
        self.extsn = extsn
        self.refrac = refrac
        self.conduc = conduc
        self.permit = permit
        self.humid = humid
        self.side_len = side_len
        self.sampling_interval = sampling_interval
        self.shrink_factor = shrink_factor
        if not self.shrink_factor or self.shrink_factor < 1:
            self.shrink_factor = 1

    def __str__(self):
        return f'RADIO NAME: {self.radio_name}\nRADIO_IS_TX: {self.radio_is_tx}\nTX HEIGHT: {self.txheight}\n' \
               f'RX HEIGHT: {self.rxheight}\nRADIO LONGITUDE: {self.radio_lon}\nRADIO LATITUDE: {self.radio_lat}\nRADIO X: {self.radio_x}\n'\
               f'RADIO Y: {self.radio_y}\nFREQUENCY: {self.freq}\nPOLARIZATION: {self.polarz}\nGENERATE FEATURES: ' \
               f'{self.generate_features}\nMAP TYPE: {self.map_type}\nMAP FILENAME: {self.map_filename}\nGAIN: {self.gain}\n' \
               f'TXPOWER: {self.txpower}\n' \
               f'EXTENSION: {self.extsn}\nREFRACTIVITY: {self.refrac}\nCONDUCTIVITY:' \
               f'{self.conduc}\nPERMITTIVITY: {self.permit}\nHUMIDITY: {self.humid}\nSIDE LENGTH: {self.side_len}\n' \
               f'SAMPLING INTERVAL: {self.sampling_interval}\n' \
               f'PARALLELIZE: {self.parallelize}\n'


    def __unicode__(self):
        s = f'RADIO NAME: {self.radio_name}\nRADIO_IS_TX: {self.radio_is_tx}\nTX HEIGHT: {self.txheight}\n' \
               f'RX HEIGHT: {self.rxheight}\nRADIO LONGITUDE: {self.radio_lon}\nRADIO LATITUDE: {self.radio_lat}\nRADIO X: {self.radio_x}\n'\
               f'RADIO Y: {self.radio_y}\nFREQUENCY: {self.freq}\nPOLARIZATION: {self.polarz}\nGENERATE FEATURES: ' \
               f'{self.generate_features}\nMAP TYPE: {self.map_type}\nMAP FILENAME: {self.map_filename}\nGAIN: {self.gain}\n' \
               f'TXPOWER: {self.txpower}\n' \
               f'EXTENSION: {self.extsn}\nREFRACTIVITY: {self.refrac}\nCONDUCTIVITY:' \
               f'{self.conduc}\nPERMITTIVITY: {self.permit}\nHUMIDITY: {self.humid}\nSIDE LENGTH: {self.side_len}\n' \
               f'SAMPLING INTERVAL: {self.sampling_interval}\n' \
               f'PARALLELIZE: {self.parallelize}\n'
        return s.encode('utf-8')
