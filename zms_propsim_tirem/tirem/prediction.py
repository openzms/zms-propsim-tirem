"""NRDZ TIREM Propagation Model."""

import os
import time
import math
import datetime
import time
import logging

import numpy as np
import pandas as pd
import scipy.io as sio

import multiprocessing
from multiprocessing import shared_memory

import zms_propsim_tirem.util.errors as errors
from zms_propsim_tirem.tirem.common import *
from zms_propsim_tirem.tirem.params import TiremParams
from zms_propsim_tirem.tirem.tirem import get_tirem_loss

LOG = logging.getLogger(__name__)

#
# futureproof fixes many issues with concurrent on WinAPI.
#
with_futures = False
if with_futures:
    import concurrent
    from concurrent.futures.process import ProcessPoolExecutor
else:
    import futureproof
    from futureproof import ProcessPoolExecutor

class ShareableNumpyArray(object):
    """
    Creates a numpy ndarray backed *possibly* with
    multiprocessing.shared_memory.SharedMemory.  Either initializes the array
    with new SharedMemory, or binds to an existing SharedMemory name.  If you
    provide a `proto` ndarray, `proto.shape`, `proto.dtype`, and `proto.nbytes`
    will be used in preference to the `shape`, `dtype`, and `nbytes` kwargs.
    If `proto` is specified and `protoinit` is `True` (the default), we will
    call `numpy.copyto` from the proto array to the new, shared memory-backed
    array.

    The intended use is for the parent (or single process runner) to create
    these as needed; then to create a new context for each `spawn`ed
    multiprocessing.Process, with detached versions of each of these.  The
    child processes will then bind to the named SharedMemory regions before
    computing.
    """
    def __init__(self, name, proto=None, protoinit=True,
                 shape=None, dtype=None, nbytes=None,
                 shared=True, shm_name=None):
        if proto is None and (not shape or not dtype or not nbytes):
            raise Exception("must provide either proto, or shape and dtype and nbytes")
        elif not proto is None and not shm_name is None:
            raise Exception(
                "do not provide proto when binding to existing shared memory;"
                " call the ShareableNumpyArray.make_attachable method in the parent instead"
                " to create a ShareableNumpyArray clone that may be attach'd in the child.")

        self.name = name
        self.shared = shared
        if not proto is None:
            self.shape = proto.shape
            self.dtype = proto.dtype
            self.nbytes = proto.nbytes
        else:
            self.shape = shape
            self.dtype = dtype
            self.nbytes = nbytes

        if not shm_name:
            if self.shared:
                LOG.debug("creating SharedMemory for %s" % (self.name,))
                self.shm = shared_memory.SharedMemory(create=True, size=self.nbytes)
                self.shm_name = self.shm.name
            else:
                self.shm = None
                self.shm_name = None
            self.array = np.ndarray(
                self.shape, dtype=self.dtype,
                buffer=self.shm.buf if self.shm else None)
            if not proto is None and protoinit:
                np.copyto(self.array, proto)
        else:
            self.shm_name = shm_name
            self.shm = None
            self.array = None

    def make_attachable(self):
        if not self.shared:
            raise Exception("cannot share when unshared")
        LOG.debug("making attachable SharedMemory for %s" % (self.name,))
        return ShareableNumpyArray(
            self.name, shape=self.shape, dtype=self.dtype, nbytes=self.nbytes,
            shm_name=self.shm_name)

    def attach(self):
        if self.shm:
            ##raise Exception("already attached")
            LOG.warn("already attached SharedMemory for %s" % (self.name,))
            return
        LOG.debug("attaching SharedMemory for %s" % (self.name,))
        self.shm = shared_memory.SharedMemory(self.shm_name)
        self.array = np.ndarray(self.shape, dtype=self.dtype, buffer=self.shm.buf)


class TiremContext(object):

    def __init__(self, params, elevation_map, tx, rx, tx_elevation, rx_elevation, wavelength):
        self.params = params
        self.elevation_map = elevation_map
        (self.nrows, self.ncols) = elevation_map.shape
        self.tx = tx
        self.rx = rx
        self.tx_elevation = tx_elevation
        self.rx_elevation = rx_elevation
        self.wavelength = wavelength
        self.start = 0
        self.end = 0
        self.elapsed = 0

        self._array_names = [
            "tirem_rssi", "distances"
        ]
        self._feature_array_names = [
            "LOS", "number_obstacles",
            "number_knife_edges", "elevation_angles_tx_rx",
            "diff_param_obstacle_with_max_h_over_r",
            "elevation_angles_NLOS", "first_last_shadowing_angles",
            "first_last_diffraction_angles", "diff_parameter"
        ]
        for name in self._array_names:
            setattr(self, name, None)
        for name in self._feature_array_names:
            setattr(self, name, None)

    def make_data(self):
        proto = np.ones((self.nrows, self.ncols)) * np.nan

        self.tirem_rssi = ShareableNumpyArray("tirem_rssi", proto=proto, shared=self.params.parallelize)
        self.distances = ShareableNumpyArray("distances", proto=proto, shared=self.params.parallelize)

        if self.params.generate_features:
            proto = np.ones((self.nrows, self.ncols))

            # line-of-sight / non-line-of-sight
            self.LOS = ShareableNumpyArray("LOS", proto=proto, shared=self.params.parallelize)

            proto = np.zeros((self.nrows, self.ncols))

            # number of blocking obstructions
            self.number_obstacles = ShareableNumpyArray("number_obstacles", proto=proto, shared=self.params.parallelize)

            # number of knife edges
            self.number_knife_edges = ShareableNumpyArray("number_knife_edges", proto=proto, shared=self.params.parallelize)

            # elevation angle between Tx & Rx with no consideration of obstacles in between
            self.elevation_angles_tx_rx = ShareableNumpyArray("elevation_angles_tx_rx", proto=proto, shared=self.params.parallelize)

            # Fresnel-Kirchhoff diffraction parameter for the obstacle with the
            # largest h (height of the obstacle above the tx-rx link) /r (fresnel
            # zone at that point) ratio
            self.diff_param_obstacle_with_max_h_over_r = ShareableNumpyArray("diff_param_obstacle_with_max_h_over_r ", proto=proto, shared=self.params.parallelize)

            proto = np.zeros((self.nrows, self.ncols, 2))

            # elevation angles considering the obstacles in between Tx & Rx
            self.elevation_angles_NLOS = ShareableNumpyArray("elevation_angles_NLOS", proto=proto, shared=self.params.parallelize)

            # shadowing angles for the first and last knife-edges
            self.first_last_shadowing_angles = ShareableNumpyArray("first_last_shadowing_angles", proto=proto, shared=self.params.parallelize)

            # diffraction angles for the first and last knife-edges
            self.first_last_diffraction_angles = ShareableNumpyArray("first_last_diffraction_angles", proto=proto, shared=self.params.parallelize)

            # Fresnel-Kirchhoff diffraction parameter
            self.diff_parameter = ShareableNumpyArray("diff_parameter", proto=proto, shared=self.params.parallelize)

    def make_attachable(self):
        ret = TiremContext(
            self.params, self.elevation_map, self.tx, self.rx,
            self.tx_elevation, self.rx_elevation, self.wavelength)
        for name in self._array_names:
            setattr(ret, name, getattr(self, name).make_attachable())
        if self.params.generate_features:
            for name in self._feature_array_names:
                setattr(ret, name, getattr(self, name).make_attachable())
        return ret

    def attach(self):
        if self.params.parallelize:
            for name in self._array_names:
                getattr(self, name).attach()
            if self.params.generate_features:
                for name in self._feature_array_names:
                    getattr(self, name).attach()


def predict(params, output_dir=".", output_prefix=None, server_job_state=None):
    """
    Code for running TIREM and generating features for augmented modeling. Adapted from SLC_TIREM_v3_Python.
    Saves tirem rssi and generated features to the current folder.
    Serhat Tadik, Aashish Gottipati, Michael A. Varner, Gregory D. Durgin

    :param params: An instance of params.TiremParams.
    :param output_dir: Defaults to the current working dir; set to None to disable output.
    :param output_prefix: A prefix for each output file; defaults to `datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")` if None.
    """

    LOG.debug("predict: params %r (%r)" % (params, server_job_state))

    if output_dir:
        if not os.access(output_dir, os.R_OK | os.W_OK | os.X_OK):
            raise errors.ConfigError("output_dir %r inaccessible" % (output_dir,))
        if output_prefix is None:
            output_prefix = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
        if output_prefix and not output_prefix.endswith("_"):
            output_prefix += "_"
        LOG.debug("output_dir valid")

    # load the map
    # Find the first non-_-prefixed key.
    map_struct = sio.loadmat(params.map_filename)
    k = '_'
    for k in map_struct.keys():
        if not k.startswith("_"):
            break
    if k.startswith('_'):
        raise errors.ConfigError("invalid matlab foundation map: must have non-_-prefixed key with data maps")
    map_struct = map_struct[k]

    ms = map_struct[0][0]
    column_map = dict(zip([name for name in ms.dtype.names], [i for i in range(len(ms.dtype.names))]))

    if params.side_len == 0:
        if "cellsize" in column_map:
            params.side_len = ms[column_map['cellsize']][0][0]
            LOG.info("side_len param not set; updating to %f (from map structure.cellsize field)" % (params.side_len,))
        else:
            raise errors.ConfigError("side_len param not set and cellsize not present in map structure; aborting")

    # :param radio_x: x coordinate of the base station in the grid; automatically calculated from radio_lon
    # :param radio_y: y coordinate of the base station in the grid; automatically calculated from radio_lat
    radio_x, radio_y, idx = lon_lat_to_grid_xy(
        np.array([params.radio_lon]), np.array([params.radio_lat]), ms, column_map) # Longitude - latitude

    LOG.debug("radio: x=%d, y=%d, idx=%d, long=%f, lat=%f" % (radio_x, radio_y, idx, params.radio_lon, params.radio_lat))

    ## Get tirem loss for all points

    # determine the tx/rx grid (raster) coordinates
    if params.radio_is_tx:
        tx = np.array([radio_x, radio_y])
        rx = None
    else:
        rx = np.array([radio_x, radio_y])
        tx = None

    # generate the map
    if params.map_type == "fusion":
        e_map = ms[column_map['dem']] + 0.3048 * ms[column_map['hybrid_bldg']]
    elif params.map_type == "lidar":
        e_map = ms[column_map['data']]

    LOG.debug("elevation map: shape=%s" % (repr(e_map.shape),))
    #LOG.debug("elevation map: order=%s" % (repr(e_map.order),))
    LOG.debug("elevation map: %s" % (repr(e_map),))

    # Initialize variables
    nrows = int(ms[column_map['nrows']])
    ncols = int(ms[column_map['ncols']])
    wavelength = 300 / params.freq

    # Determine tx / rx height from the map
    if params.radio_is_tx:
        tx_elevation = e_map[tx[1], tx[0]]
        rx_elevation = None
    else:
        tx_elevation = None
        rx_elevation = e_map[rx[1], rx[0]]

    LOG.debug("ncols=%d, nrows=%d" % (ncols, nrows))

    #params.shrink_factor = 20
    #params.shrink_factor = 1
    #params.shrink_factor = 100
    if not params.shrink_factor or params.shrink_factor < 1:
        params.shrink_factor = 1
    n_ncols = int(ncols/params.shrink_factor)
    n_nrows = int(nrows/params.shrink_factor)

    LOG.debug("n_ncols=%d, n_nrows=%d" % (n_ncols, n_nrows))

    ## Run TIREM
    if params.parallelize and (params.parallelize == True or params.parallelize < 0):
        # XXX: 61 seems to be a hardcoded limit inside multiprocessing?!
        params.parallelize = min(multiprocessing.cpu_count(), 61)

    ctx = TiremContext(params, e_map, tx, rx, tx_elevation, rx_elevation, wavelength)
    ctx.make_data()

    ctx.start = time.time()

    if server_job_state:
        server_job_state.set_context(ctx)

    # XXX: want to have (many) more chunks that workers so that slow
    # chunks/cores do not stall main process unduly.

    # Loop through the map pixels
    if params.parallelize:
        # Some chunks seem to slow way down, so ensure that no slow chunks
        # unduly clog the system, as could happen if num_chunks == max_workers.
        # However, if chunks finish fast enough, then it's not worth it to
        # scale down, so if the params.shrink_factor is >= 15, probably best to keep
        # this at 1.5 max.
        #
        # However, this is also related to the cost to spawn each worker
        # process, and due to some issue with multiprocessing and/or our binary
        # libs, we have to let each worker die and be re-spawned (see
        # max_tasks_per_child below).  In other words, you want chunks to live
        # long enough to reach full parallelism CPU utilization within two
        # minutes into the run, to keep the pipeline as full as possible.  If
        # chunks finish too soon, this will not happen.
        #
        # XXX: we want the min chunk size to be 1000-3000 cells.
        if params.shrink_factor > 15:
            chunk_scaledown_factor = 1.5
        elif params.shrink_factor > 4:
            chunk_scaledown_factor = 3
        elif params.shrink_factor > 1:
            chunk_scaledown_factor = 12
        else:
            chunk_scaledown_factor = 32
        #chunk_scaledown_factor = 10
        # This code is windows-only, but the paradigm is spawn, so just in case
        # we ever have a Linux TIREM shlib, set expectations.  The current design
        # may not work with the `fork` method.
        try:
            multiprocessing.set_start_method('spawn')
        except:
            pass

        attachable_ctx = ctx.make_attachable()
        # XXX: assume squarish
        chunk_size = max(int(math.ceil(n_ncols / params.parallelize / chunk_scaledown_factor)), 1)
        chunks = min(int(math.ceil(n_ncols / chunk_size)), n_ncols)
        max_workers = min(params.parallelize, chunks)
        LOG.debug("chunk_size = %r, chunks = %r, max_workers=%d" % (chunk_size, chunks, max_workers))

        # concurrent.futures is *not* up to high numbers of task lists.  It
        # seemed buggy on windows (wine) after 180 tasks or so.
        # concurrent.futures would (apparently) hang, and refuse to execute
        # workers with new tasks after the initial set.  It seemed that the
        # initial task submission would nearly immediately overlap with worker
        # process init, and I assume this is causing a problem.
        #
        # futureproof does not suffer from this symptom; it also does a better
        # job at not eating exceptions, etc.
        # https://futureproof.readthedocs.io/en/latest/api.html
        # https://stackoverflow.com/questions/70270814/pythons-concurrent-futures-hangs-in-both-multithreading-and-multiprocessing-on
        #
        if with_futures:
            plist = []
            with ProcessPoolExecutor(max_workers=max_workers, max_tasks_per_child=1) as executor:
                for i in range(0, chunks):
                    col_start = i * chunk_size
                    num_cols = (n_ncols - col_start) if ((col_start + chunk_size) > n_ncols) else chunk_size
                    LOG.debug("chunk: col_start = %d, num_cols = %d" % (col_start, num_cols))
                    #p = multiprocessing.Process(target=tirem_pred_chunk_safe, args=(attachable_ctx, col_start, num_cols, n_nrows))
                    #plist.append(p)
                    #p.start()
                    plist.append(executor.submit(tirem_pred_chunk_safe, attachable_ctx, col_start, num_cols, n_nrows))
            chunks_completed = 0
            while len(plist):
                wlen = min(4, len(plist))
                (retplist, _) = concurrent.futures.wait(plist[0:wlen])
                chunks_completed += wlen
                LOG.debug("chunks completed: %.2f%% (%d/%d)" % (100.0 * chunks_completed / chunks, chunks_completed, chunks))
                if len(plist) == wlen:
                    break
                plist = plist[wlen:]
        else:
            executor = futureproof.ProcessPoolExecutor(max_workers=max_workers) #, max_tasks_per_child=1)
            with futureproof.TaskManager(executor) as tm:
                chunks_completed = 0
                for i in range(0, chunks):
                    col_start = i * chunk_size
                    num_cols = (n_ncols - col_start) if ((col_start + chunk_size) > n_ncols) else chunk_size
                    LOG.debug("chunk: col_start = %d, num_cols = %d" % (col_start, num_cols))
                    tm.submit(tirem_pred_chunk_safe, attachable_ctx, col_start, num_cols, n_nrows)
                for task in tm.as_completed():
                    chunks_completed += 1
                    completion_percentage = 100.0 * chunks_completed / chunks
                    LOG.debug("[----] chunks completed: %.2f%% (%d/%d)",
                              completion_percentage, chunks_completed, chunks)
                    if server_job_state:
                        server_job_state.update_completion_sync(completion_percentage)
    else:
        tirem_pred_chunk_safe(ctx, 0, n_ncols, n_nrows)

    ctx.end = time.time()
    ctx.elapsed = time.time() - ctx.start
    LOG.debug("elapsed time: " + str(ctx.elapsed))

    if output_dir:
        prefix = output_dir + os.path.sep + output_prefix

        def save_output(name, array):
            fname = name + ".npy"
            np.save(prefix + fname, ctx.tirem_rssi.array)
            if server_job_state:
                server_job_state.add_url_data_output(
                    name=name, filename=output_prefix + fname)

        def save_image(name, array):
            from osgeo import gdal, osr
            gdal.UseExceptions()

            fname = name + ".tiff"

            no_data_val = int(np.nanmin(array)) - 1 # array.min().round())
            narray = np.nan_to_num(array, no_data_val)
            narray = narray[::-1]

            zone = ms[column_map['utmZoneNum']][0][0]
            zoneLtr = ms[column_map['utmZoneLtr']][0][0]
            (w, e, s, n) = ms[column_map['axis']][0]
            (long, lat) = utm_to_wgs84(w, n, zone, zoneLtr)
            (longb, latb) = utm_to_wgs84(e, s, zone, zoneLtr)
            (ncols, nrows) = (ms[column_map['ncols']][0][0], ms[column_map['nrows']][0][0])
            #longres = (longb - long) / ncols
            #latres = (latb - lat) / nrows
            longres = ((e - w) / ncols)/100000.0
            latres = ((s - n) / nrows)/100000.0

            LOG.debug("save_image: narray.shape=%r,zone=%d,zoneLtr=%s,w=%f,e=%f,s=%f,n=%f,ncols=%d,nrows=%d,long=%f,longb=%f,lat=%f,latb=%f,longres=%f,latres=%f" % (narray.shape, zone, zoneLtr, w, e, s, n, ncols, nrows, long, longb, lat, latb, longres, latres,))

            driver = gdal.GetDriverByName("GTiff")
            if narray.dtype == np.float64:
                gtype = gdal.GDT_Float64
            elif narray.dtype == np.float32:
                gtype = gdal.GDT_Float32
            else:
                gtype = gdal.GDT_Int16
            img = driver.Create(prefix + fname, narray.shape[1], narray.shape[0], 1, gtype) #, options=['TILED=YES'])
            img.SetGeoTransform((long, longres, 0.0, lat, 0.0, latres))
            t_srs = osr.SpatialReference()
            #t_srs.ImportFromEPSG(4326)
            t_srs.ImportFromProj4('+proj=longlat +ellps=WGS84 +units=m +datum=WGS84 +no_defs')
            img.SetProjection(t_srs.ExportToWkt())
            band = img.GetRasterBand(1)
            band.SetNoDataValue(no_data_val)
            band.WriteArray(narray)
            band.FlushCache()
            band.ComputeStatistics(False)
            img.FlushCache()

            if server_job_state:
                server_job_state.add_url_map_output(
                    name=name, filename=output_prefix + fname)

        save_output("tirem_rssi", ctx.tirem_rssi.array)
        try:
            save_image("tirem_rssi", ctx.tirem_rssi.array)
        except:
            LOG.exception("error saving map")

        save_output("distances", ctx.distances.array)

        if params.generate_features:
            save_output('LOS_NLOS', ctx.LOS.array)
            save_output('number_obstacles', ctx.number_obstacles.array)
            save_output('number_knife_edges', ctx.number_knife_edges.array)
            save_output('elevation_angles_tx_rx', ctx.elevation_angles_tx_rx.array)
            save_output('elevation_angles_NLOS', ctx.elevation_angles_NLOS.array)
            save_output('shadowing_angles', ctx.first_last_shadowing_angles.array)
            save_output('diffraction_angles', ctx.first_last_diffraction_angles.array)
            save_output('diff_parameter', ctx.diff_parameter.array)
            save_output('diff_param_obstacle_with_max_h_over_r', ctx.diff_param_obstacle_with_max_h_over_r.array)

    return ctx

def tirem_pred_chunk_safe(ctx, chunk_col_start, chunk_num_cols, chunk_nrows):
    try:
        return tirem_pred_chunk(ctx, chunk_col_start, chunk_num_cols, chunk_nrows)
    except:
        import traceback
        traceback.print_exc()
    return

def tirem_pred_chunk(ctx, chunk_col_start, chunk_num_cols, chunk_nrows):
    pid = os.getpid()

    LOG.debug("[%d] init: start=%d, n=%d, rows=%d",
              pid, chunk_col_start, chunk_num_cols, chunk_nrows)

    ctx.attach()
    chunk_col_end = chunk_col_start + chunk_num_cols
    chunk_total = chunk_num_cols * chunk_nrows
    chunk_update_mod = max(int(chunk_total / 10), 1)

    tx = ctx.tx
    rx = ctx.rx

    LOG.debug("[%d] start: start=%d, n=%d, rows=%d, total=%d, mod=%d",
              pid, chunk_col_start, chunk_num_cols, chunk_nrows, chunk_total, chunk_update_mod)

    for a in range(chunk_col_start, chunk_col_end):

        for b in range(chunk_nrows):

            # Display progress
            chunks_complete = (a - chunk_col_start) * chunk_nrows + b
            if chunks_complete % chunk_update_mod == 0:
                LOG.debug("[%d] progress: %.2f%% (complete=%d, total=%d)",
                          pid, 100.0 * chunks_complete / chunk_total,
                          chunks_complete, chunk_total)

            flag = 0
            cntr = 0
            if ctx.params.radio_is_tx:
                rx = np.array([a, b]) + np.array([1, 1])
                ctx.rx_elevation = ctx.elevation_map[b, a]
            else:
                tx = np.array([a, b]) + np.array([1, 1])
                ctx.tx_elevation = ctx.elevation_map[b, a]
            if (rx == tx).all() == 0:
                # build arrays and calculate TIREM's predictions
                [d_array, e_array] = build_arrays(ctx.params.side_len, ctx.params.sampling_interval, tx, rx, ctx.elevation_map)
                ctx.tirem_rssi.array[b, a] = ctx.params.txpower + ctx.params.gain - get_tirem_loss(d_array, e_array, ctx.params)
                dir_ = rx - tx
                ctx.distances.array[b, a] = np.linalg.norm(dir_, 2) * ctx.params.side_len

                if ctx.params.generate_features:
                    # Get the nonzero elements of the distance and elevation arrays

                    nonzero_e_array_indices = [e_array != 0]
                    d_array_nonzero = d_array[tuple(nonzero_e_array_indices)]
                    e_array_nonzero = e_array[tuple(nonzero_e_array_indices)]

                    # Calculate the total Rx and Tx heights and the slope between them
                    rx_total_height = ctx.rx_elevation + ctx.params.rxheight
                    tx_total_height = ctx.tx_elevation + ctx.params.txheight
                    slope = (rx_total_height - tx_total_height) / max(d_array_nonzero)

                    # Elevation angle
                    ctx.elevation_angles_tx_rx.array[b, a] = 90 - math.degrees(math.atan(slope))

                    # Blocking obstruction information, bo_info, initialization
                    bo_info = np.array([[0, 0, 0]])

                    for i in range(len(e_array_nonzero) - 1):
                        cntr = cntr + 1
                        # if there is a blockage, LOS = 0, (NLOS = 1)
                        if (d_array_nonzero[i] * slope + tx_total_height) < (e_array_nonzero[i]):
                            ctx.LOS.array[b, a] = 0
                            # if it's the first obstacle, calculate the diffraction parameter
                            if flag == 0:
                                h = e_array_nonzero[i] - (d_array_nonzero[i] * slope + tx_total_height)
                                d1 = max(np.sqrt((d_array_nonzero[i] * slope) ** 2 + d_array_nonzero[i] ** 2), 0.25)
                                d2 = max(np.sqrt(((d_array_nonzero[-1] - d_array_nonzero[i]) * slope) ** 2 + (d_array_nonzero[-1] - d_array_nonzero[i]) ** 2), 0.25)
                                ctx.diff_parameter.array[b, a, 0] = h * np.sqrt(2 * (d1 + d2) / (ctx.wavelength * d1 * d2))
                            # calculate the number of obstacles and fill the bo_info with distance, elevation, h, and
                            # r(Fresnel zone) information of obstacles
                            if flag == 0 or cntr > 200:
                                ctx.number_obstacles.array[b, a] = ctx.number_obstacles.array[b, a] + 1
                                cntr = 0
                                h = e_array_nonzero[i] - (d_array_nonzero[i] * slope + tx_total_height)
                                r = np.sqrt(ctx.wavelength * d_array_nonzero[i] * (d_array_nonzero[-1] - d_array_nonzero[i])) * np.sqrt(1 + slope ** 2) / d_array_nonzero[-1]
                                bo_info = np.append(bo_info, [[d_array_nonzero[i], h, r]], 0)
                                if flag == 0:
                                    bo_info = np.delete(bo_info, 0, 0)

                                flag = 1


                    # find the obstacle with largest h / r ratio and calculate the diffraction parameter for it
                    if (bo_info == np.array([[0, 0, 0]])).all() == 0:
                        idxx = (bo_info[:, 1] / bo_info[:, 2]) == max(bo_info[:, 1] / bo_info[:, 2])
                        d1_ = max(bo_info[idxx, 0], 0.25)
                        h_ = bo_info[idxx, 1]
                        ctx.diff_param_obstacle_with_max_h_over_r.array[b, a] = h_ * np.sqrt(2 * (d_array_nonzero[-1] * (np.sqrt(1 + slope ** 2))) / (ctx.wavelength * d1_ * (d_array_nonzero[-1] - d1_) * (1 + slope ** 2)))

                    # knife edge information, ke_info, initialization
                    ke_info = np.array([[0, 0]])
                    flag = 0
                    cntr = 0
                    d_ke = 0
                    for i in range(len(e_array_nonzero) - 1):
                        cntr = cntr + 1
                        # different from blocking obstacle, change the slope each time you come across an obstacle
                        if ((d_array_nonzero[i] - d_ke) * slope + tx_total_height) < (e_array_nonzero[i]):
                            # calculate number of knife edges
                            if flag == 0 or cntr > 200:
                                ctx.number_knife_edges.array[b, a] += 1
                                # if it's the first ke, calculate the shadowing angle
                                if flag == 0:
                                    ctx.first_last_shadowing_angles.array[b, a, 0] = math.degrees(math.atan(
                                        (e_array_nonzero[i] - tx_total_height) / d_array_nonzero[i])) - math.degrees(
                                        math.atan(slope))

                                # update ke_info with distance and elevation information
                                ke_info = np.append(ke_info, [[d_array_nonzero[i], e_array_nonzero[i]]], 0)
                                if flag == 0:
                                    ke_info = np.delete(ke_info, 0, 0)
                                # update parameters
                                d_ke = d_array_nonzero[i]
                                tx_total_height = e_array_nonzero[i]
                                slope = (rx_total_height - tx_total_height) / (max(d_array_nonzero) - d_array_nonzero[i])

                                cntr = 0
                                flag = 1

                    # redefine the original slope in case it changes in the previous loop
                    rx_total_height = ctx.rx_elevation + ctx.params.rxheight
                    tx_total_height = ctx.tx_elevation + ctx.params.txheight
                    slope = (rx_total_height - tx_total_height) / max(d_array_nonzero)

                    # if there is at least one ke, calculate NLOS elevation angles, shadowing angle for the
                    # last ke, and diffraction angles.
                    if (ke_info == np.array([[0, 0]])).all() == 0:

                        h = ke_info[-1, 1] - ((ke_info[-1, 0] - d_array_nonzero[-1]) * slope + rx_total_height)
                        d1 = max(np.sqrt(((ke_info[-1, 0] - d_array_nonzero[-1]) * slope) ** 2 + (ke_info[-1, 0] - d_array_nonzero[-1]) ** 2), 0.25)
                        d2 = max(np.sqrt(((d_array_nonzero[0] - ke_info[-1, 0]) * slope) ** 2 + (d_array_nonzero[0] - ke_info[-1, 0]) ** 2), 0.25)

                        ctx.diff_parameter.array[b, a, 1] = h * np.sqrt(2 * (d1 + d2) / (ctx.wavelength * d1 * d2))
                        ctx.first_last_shadowing_angles.array[b, a, 1] = math.degrees(math.atan((ke_info[-1, 1] - rx_total_height) / (d_array_nonzero[-1] - ke_info[-1, 0]))) + math.degrees(math.atan(slope))
                        ctx.elevation_angles_NLOS.array[b, a, 0] = 90 - math.degrees(math.atan((ke_info[0, 1] - tx_total_height) / max((ke_info[0, 0] - d_array_nonzero[0]),0.25)))
                        ctx.elevation_angles_NLOS.array[b, a, 1] = 90 - math.degrees(math.atan((ke_info[-1, 1] - rx_total_height) / max((d_array_nonzero[-1] - ke_info[-1, 0]),0.25)))

                        if ke_info.shape[0] == 1:
                            slope_ke_to_rx = (ke_info[0, 1] - rx_total_height) / (d_array_nonzero[-1] - ke_info[0, 0])
                            ctx.first_last_diffraction_angles.array[b, a, 0] = math.degrees(math.atan((ke_info[0, 1] - tx_total_height) / ke_info[0, 0])) + math.degrees(math.atan(slope_ke_to_rx))
                            ctx.first_last_diffraction_angles.array[b, a, 1] = ctx.first_last_diffraction_angles.array[b, a, 0]

                        if ke_info.shape[0] != 1:

                            slope_1 = (ke_info[0, 1] - tx_total_height) / ke_info[0, 0]
                            slope_2 = (ke_info[0, 1] - ke_info[1, 1]) / (ke_info[1, 0] - ke_info[0, 0])
                            ctx.first_last_diffraction_angles.array[b, a, 0] = math.degrees(math.atan(slope_1)) + math.degrees(math.atan(slope_2))

                            # if the encountered obstacle isn't actually an obstacle that the waves would diffract from
                            # (e.g.the first encountered obstacle doesn't block the link between the Tx and the second
                            # obstacle), correct the variables
                            cnt_ke = 0
                            cnt2 = 0
                            while cnt_ke < ke_info.shape[0]:
                                cnt2 = cnt2 + 1
                                cnt_ke = cnt_ke + 1
                                if cnt_ke == 1:
                                    slope_1 = (ke_info[cnt_ke-1, 1] - tx_total_height) / ke_info[cnt_ke-1, 0]
                                else:
                                    slope_1 = (ke_info[cnt_ke-1, 1] - ke_info[cnt_ke - 2, 1]) / (ke_info[cnt_ke-1, 0] - ke_info[cnt_ke - 2, 0])

                                if cnt_ke == ke_info.shape[0]:
                                    slope_2 = (ke_info[cnt_ke-1, 1] - rx_total_height) / (d_array_nonzero[-1] - ke_info[cnt_ke-1, 0])
                                else:
                                    slope_2 = (ke_info[cnt_ke-1, 1] - ke_info[cnt_ke, 1]) / (ke_info[cnt_ke , 0] - ke_info[cnt_ke-1, 0])
                                cnt = cnt_ke
                                idx = 0
                                while math.degrees(math.atan(slope_1)) < -1 * math.degrees(math.atan(slope_2)):
                                    idx = idx + 1
                                    if ke_info.shape[0] == cnt + 1:

                                        if ke_info.shape[0]- 1 - idx == 0:
                                            slope_1 = (ke_info[-1, 1] - tx_total_height) / ke_info[-1, 0]
                                        else:
                                            slope_1 = (ke_info[-1, 1] - ke_info[-2 - idx, 1]) / (ke_info[-1, 0] - ke_info[- 2 - idx, 0])

                                        slope_2 = (ke_info[-1, 1] - rx_total_height) / (d_array_nonzero[-1] - ke_info[-1, 0])
                                    else:
                                        if cnt2 == 1:
                                            slope_1 = (ke_info[cnt, 1] - tx_total_height) / ke_info[cnt, 0]
                                        else:
                                            slope_1 = (ke_info[cnt, 1] - ke_info[cnt_ke - idx - 1, 1]) / (ke_info[cnt, 0] - ke_info[cnt_ke - idx - 1, 0])

                                        slope_2 = (ke_info[cnt, 1] - ke_info[cnt + 1, 1]) / (
                                                    ke_info[cnt + 1, 0] - ke_info[cnt , 0])

                                    if cnt2 == 1:
                                        h__ = ke_info[cnt, 1] - (ke_info[cnt, 0] * slope + tx_total_height)
                                        d1__ = math.sqrt((ke_info[cnt, 0] * slope) ** 2 + ke_info[cnt, 0] ** 2)
                                        d2__ = math.sqrt(((d_array_nonzero[-1] - ke_info[cnt, 0]) * slope) ** 2 + (d_array_nonzero[-1] - ke_info[cnt , 0]) ** 2)
                                        ctx.diff_parameter.array[b, a, 0] = h__ * math.sqrt(2 * (d1__ + d2__) / (ctx.wavelength * d1__ * d2__))

                                        ctx.first_last_diffraction_angles.array[b, a, 0] = math.degrees(math.atan(slope_1)) + math.degrees(math.atan(slope_2))
                                        ctx.first_last_shadowing_angles.array[b, a, 0] = math.degrees(math.atan(slope_1))

                                    cnt = cnt + 1
                                    cnt_ke = cnt_ke + 1
                                    ctx.number_knife_edges.array[b, a] = ctx.number_knife_edges.array[b, a] - 1
                                    if ctx.number_knife_edges.array[b, a] < 0:
                                        LOG.debug(ctx.number_knife_edges.array[b, a])

                            if ctx.first_last_diffraction_angles.array[b, a, 0] < 0:
                                LOG.debug(ctx.first_last_diffraction_angles.array[b, a, 0])


                            slope_last_ke_to_rx = (ke_info[-1, 1] - rx_total_height) / (d_array_nonzero[-1] - ke_info[-1, 0])
                            slope_ke_beforelast_to_ke_last = (ke_info[-2, 1] - ke_info[-1, 1]) / (ke_info[-1, 0] - ke_info[-2, 0])
                            ctx.first_last_diffraction_angles.array[b, a, 1] = math.degrees(math.atan(slope_last_ke_to_rx)) - math.degrees(math.atan(slope_ke_beforelast_to_ke_last))

    LOG.debug("[%d] progress: %.2f%%", pid, 100.0)
