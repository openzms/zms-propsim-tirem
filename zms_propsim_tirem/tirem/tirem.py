
import logging
from ctypes import *

import numpy as np

LOG = logging.getLogger(__name__)
tirem_dll = None

def load_tirem_lib():
    """Loads tirem DLL"""
    global tirem_dll

    try:
        tirem_dll = CDLL("./TIREM320DLL.dll")
    except OSError:
        try:
            tirem_dll = WinDLL("./TIREM320DLL.dll")
        except OSError:
            LOG.exception('ERROR! Failed to load TIREM DLL')


def call_tirem_loss(d_array, e_array, params):
    """Sets up data for Tirem DLL call."""
    # Load DLL
    if tirem_dll is None:
        load_tirem_lib()

    # initialize the pointer and data for each argument
    # inputs just set to some number in their valid range
    TANTHT = pointer(c_float(params.txheight))  # 0 - 30, 000m
    RANTHT = pointer(c_float(params.rxheight))
    PROPFQ = pointer(c_float(params.freq))  # 1 to 20, 000 MHz

    # next three values characterize the shape of terrain
    NPRFL = pointer(c_int32(d_array.shape[0]))  # number of points in array MAYBE TGUS

    HPRFL = e_array.astype(np.float32).ctypes.data_as(POINTER(c_float))  # array of above (mean) sea level heights
    XPRFL = d_array.astype(np.float32).ctypes.data_as(POINTER(c_float))  # array of great circles distances between
    # points and start

    EXTNSN = pointer(c_int32(params.extsn))  # boolean, 0 is false
    # anything else is true. False = new profile, true = extension of last profile terrain
    # Haven't been able to figure out what the extension flag actually does

    REFRAC = pointer(c_float(params.refrac))  # Surface refractivity  200 to 450.0 "N-Units"
    CONDUC = pointer(c_float(params.conduc))  # 0.00001 to 100.0 S/m
    PERMIT = pointer(c_float(params.permit))  # Relative permittivity of earth surface  1 to 1000
    HUMID = pointer(c_float(params.humid))  # Units g/m^3   0 to 110.0
    polar_ascii = np.array([ord(c) for c in params.polarz])
    POLARZ = polar_ascii.astype(np.uint8).ctypes.data_as(POINTER(c_void_p))

    # output starts here, I just intialize them all to 0
    VRSION = np.array([0, 0, 0, 0, 0, 0, 0, 0], dtype=np.uint8).ctypes.data_as(POINTER(c_void_p))
    MODE = np.array([0, 0, 0, 0], dtype=np.uint8).ctypes.data_as(POINTER(c_void_p))
    LOSS = pointer(c_float(0))
    FSPLSS = pointer(c_float(0))

    tirem_dll.CalcTiremLoss(TANTHT, RANTHT, PROPFQ, NPRFL, HPRFL, XPRFL, EXTNSN, REFRAC, CONDUC,
                            PERMIT, HUMID, POLARZ, VRSION, MODE, LOSS, FSPLSS)
    return LOSS.contents.value


def get_tirem_loss(d_array, e_array, params):
    """Returns TIREM loss.

    Usage: loss = get_tirem_loss(d_array, e_array, params)

    inputs: d_array - distance array
            e_array - elevation array
            params - transmitter parameters

    output:   loss - estimated propagation loss
    """
    return call_tirem_loss(d_array, e_array, params)
