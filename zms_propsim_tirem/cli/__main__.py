import argparse
import sys
import logging

import zms_propsim_tirem.tirem.params as tirem_params
from zms_propsim_tirem.tirem.params import TiremParams
from zms_propsim_tirem.tirem.prediction import predict
from zms_propsim_tirem.util.log import configure_logging

LOG = logging.getLogger(__name__)

def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--radio-name', default="manual",
                        help="Radio name")
    parser.add_argument('--is-rx', action="store_true",
                        help="Radio is receiver, not transmitter.")
    parser.add_argument('--tx-height', default=3.0, type=float,
                        help="Transmitter height (m) (do not include the building height or elevation, just the tx height above the surface it rests upon)")
    parser.add_argument('--rx-height', default=1.5, type=float,
                        help="Receiver height (m) (do not include the building height or elevation, just the height above the surface it rests upon)")
    parser.add_argument('--radio-lon', required=True, type=float,
                        help="Radio longitude (WGS84)")
    parser.add_argument('--radio-lat', required=True, type=float,
                        help="Radio latitude (WGS84)")
    parser.add_argument('--freq', required=True, type=float,
                        help="Transmission frequency (MHz)")
    parser.add_argument('--polarz', default="H",
                        help="Polarization of the wave produced by the tx antenna ('H' = horizontal polarization; 'V' = vertical polarization)")
    parser.add_argument('--map-type', default=tirem_params.MAP_TYPE,
                        help='Fusion map ("fusion") or lidar DSM map ("lidar"). The fusion map struct has to have the fields "dem" for digital elevation map and "hybrid_bldg" for building heights. The lidar map has to have the field "data" having combined information of elevations and building heights.')
    parser.add_argument('--map-filename', default=tirem_params.MAP_FILENAME,
                        help='Map filename.')
    parser.add_argument('--gain', type=float, default=tirem_params.GAIN,
                        help="Antenna gain (dB)")
    parser.add_argument('--txpower', type=float, default=tirem_params.TXPOWER,
                        help="Transmitter power (dBm)")
    parser.add_argument('--refrac', type=float, default=tirem_params.REFRAC,
                        help="Surface refractivity, range: (200 to 450.0) 'N-Units'")
    parser.add_argument('--conduc', type=float, default=tirem_params.CONDUC,
                        help="Conductivity, range: (0.00001 to 100.0) S/m")
    parser.add_argument('--permit', type=float, default=tirem_params.PERMIT,
                        help="Relative permittivity of earth surface, range: (1 to 1000)")
    parser.add_argument('--humid', type=float, default=tirem_params.HUMID,
                        help="Humidity, units g/m^3, range: (0 to 110.0)")
    parser.add_argument('--side-len', type=float, default=tirem_params.SIDE_LEN,
                        help="Grid cell side length (m).  This should be provided in the cellsize field in the map data structure, but can be overridden here.")
    parser.add_argument('--sampling-interval', type=float, default=tirem_params.SAMPLING_INTERVAL,
                        help="Sampling interval along the tx-rx link (e.g. 0.5 means a given grid cell is sampled is sampled twice; side length of 0.5 and sampling interval of 0.5 mean that the Tx-Rx horizontal array step size 0.5*0.5 = 0.25 m.)")
    parser.add_argument('--parallelize', type=int, default=tirem_params.PARALLELIZE,
                        help="The number of parallel TIREM processes to spawn and run; set to -1 to parallelize amongst multiprocessing.cpu_count() subtasks (or max of 61 on Windows); set to a specific integer number of subtasks; or set to 0 to disable parallel, multi-process execution")
    parser.add_argument('--generate-features', action="store_true",
                        help="Generate features in addition to TIREM path loss predictions")
    parser.add_argument('--shrink-factor', default=1, type=int,
                        help="Compute only the first shrink_factor columns and shrink_factor rows.  If shrink_factor is 20, only computes 1/400 of the job.  Useful only for debugging.")
    
    args = parser.parse_args(sys.argv[1:])
    
    configure_logging(debug=True)

    tp = TiremParams(
        args.radio_name, not args.is_rx, args.tx_height, args.rx_height,
        args.radio_lon, args.radio_lat, args.freq, args.polarz,
        map_type=args.map_type, map_filename=args.map_filename,
        gain=args.gain, txpower=args.txpower,
        refrac=args.refrac, conduc=args.conduc, permit=args.permit, humid=args.humid,
        side_len=args.side_len, sampling_interval=args.sampling_interval,
        parallelize=args.parallelize, generate_features=args.generate_features,
        shrink_factor=args.shrink_factor)
    LOG.debug("params: \n" + str(tp))
    run_date = predict(tp, output_dir="C:\\data")

if __name__ == "__main__":
    main()
