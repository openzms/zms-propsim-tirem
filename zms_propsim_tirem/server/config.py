import sys
import os
import argparse
import uuid
import logging

LOG = logging.getLogger(__name__)

class Config(object):

    def __init__(self):
        self.identity_rpc_endpoint = None
        self.controller_rpc_endpoint = None
        self.rpc_endpoint = "0.0.0.0:8055"
        self.service_id = str(uuid.uuid4())
        self.service_name = "propsim-tirem"
        self.service_description = "propsim-tirem"
        self.http_file_server_urlbase = None
        self.http_file_server_local_dir = None
        self.debug = False

        self._identity_rpc_endpoint_host = None
        self._identity_rpc_endpoint_host = None
        self._controller_rpc_endpoint_port = None
        self._controller_rpc_endpoint_port = None

        self._parser = argparse.ArgumentParser()
        self._parser.add_argument(
            "--identity-rpc-endpoint", type=str,
            default=os.getenv("IDENTITY_RPC_ENDPOINT", self.identity_rpc_endpoint),
            help="The Identity service gRPC endpoint.")
        self._parser.add_argument(
            "--controller-rpc-endpoint", type=str,
            default=os.getenv("CONTROLLER_RPC_ENDPOINT", self.controller_rpc_endpoint),
            help="The Controller service gRPC endpoint.")
        self._parser.add_argument(
            "--rpc-endpoint", type=str,
            default=os.getenv("RPC_ENDPOINT", self.rpc_endpoint),
            help="The Controller service gRPC endpoint.")
        self._parser.add_argument(
            "--service-id", type=str,
            default=os.getenv("SERVICE_ID", self.service_id),
            help="The service id.")
        self._parser.add_argument(
            "--service-name", type=str,
            default=os.getenv("SERVICE_NAME", self.service_name),
            help="The service name.")
        self._parser.add_argument(
            "--service-description", type=str,
            default=os.getenv("SERVICE_DESCRIPTION", self.service_description),
            help="The service description.")
        self._parser.add_argument(
            "--http-file-server-urlbase", type=str,
            default=os.getenv("HTTP_FILE_SERVER_URLBASE", self.http_file_server_urlbase),
            help="An associated HTTP file server base url that can serve files on our behalf; assumes a cross-container shared filesystem (http-file-server-local-dir).")
        self._parser.add_argument(
            "--http-file-server-local-dir", type=str,
            default=os.getenv("HTTP_FILE_SERVER_LOCAL_DIR", self.http_file_server_local_dir),
            help="The local dir that is reachable via http-file-server-urlbase.")
        self._parser.add_argument(
            "--debug", action="store_true",
            default=os.getenv("DEBUG", self.debug),
            help="Enable debug mode.")

        self._fields = [
            'identity_rpc_endpoint', 'controller_rpc_endpoint', 'rpc_endpoint',
            'service_id', 'service_name', 'service_description', 'http_file_server_urlbase',
            'http_file_server_local_dir', 'debug'
        ]
        self._endpoint_fields = [
            'identity_rpc_endpoint', 'controller_rpc_endpoint', 'rpc_endpoint'
        ]

        self._args = None

    @property
    def identity_rpc_endpoint_host(self):
        return self._identity_rpc_endpoint_host

    @property
    def identity_rpc_endpoint_port(self):
        return self._identity_rpc_endpoint_port

    @property
    def controller_rpc_endpoint_host(self):
        return self._controller_rpc_endpoint_host

    @property
    def controller_rpc_endpoint_port(self):
        return self._controller_rpc_endpoint_port

    @property
    def rpc_endpoint_host(self):
        return self._rpc_endpoint_host

    @property
    def rpc_endpoint_port(self):
        return self._rpc_endpoint_port

    def load(self, argv=sys.argv):
        self._args = self._parser.parse_args(argv)

        for field in self._fields:
            av = getattr(self._args, field)
            LOG.debug("field %r = %r", field, av)
            setattr(self, field, av)
        for field in self._endpoint_fields:
            v = getattr(self._args, field)
            if not v:
                continue
            try:
                (h, p) = v.split(":")
                p = int(p)
            except:
                raise ValueError("malformed %s value %r: should be <host-or-ip>:<port> string" % (field, v))
            else:
                setattr(self, "_" + field + "_host", h)
                setattr(self, "_" + field + "_port", p)
