
import asyncio
import platform
import logging
import os
import uuid
import time
import functools

import grpclib.client
import grpclib.server
import grpclib.utils
from grpclib.exceptions import GRPCError
from grpclib.const import Status
from google.protobuf.timestamp_pb2 import Timestamp

from protoc_gen_validate.validator import validate, ValidationFailed, validate_all

import zms_api.identity.v1.identity_pb2 as identity_pb2
import zms_api.identity.v1.identity_grpc as identity_grpc
import zms_api.propsim.v1.propsim_pb2 as propsim_pb2
import zms_api.propsim.v1.propsim_grpc as propsim_grpc

import zms_propsim_tirem.version as version
import zms_propsim_tirem.util.errors as errors

from zms_propsim_tirem.tirem.params import TiremParams
from zms_propsim_tirem.tirem.prediction import predict

LOG = logging.getLogger(__name__)

class JobOutputState(object):
    """A wrapper class that encapsulates JobOutputs."""
    def __init__(self, job_output, path):
        self.job_output = job_output
        self.path = path

    def delete(self):
        os.unlink(self.path)
        self.path = None

class IterableAsyncQueue(asyncio.Queue):
    """A simple async generative iterator atop asyncio.Queue.get() to help implement the event producer/consumer pattern that is ideal for notifying stream listener clients."""
    def __aiter__(self):
        return self

    async def __anext__(self):
        return await self.get()

class JobState(object):
    """A wrapper class that encapsulates Jobs and their runtime state.  Provides helpers to notify clients listening for JobEvents."""

    def __init__(self, config, job, params):
        self.config = config
        self.job = job
        self.params = params
        self.context = None
        self.event_queues = dict()
        self.outputs = dict()
        self._output_dir = self.config.http_file_server_local_dir + os.path.sep + self.job.id
        #
        # If propsim core is CPU bound in Python, as is the TIREM-based
        # simulation in this repo, we will inevitably need to place it on
        # another thread, and deliver completion updates or other events back
        # to the async context that created this job.  Thus, we save the event
        # loop that created us, and assume that is the right loop to reinject
        # async notifications back to clients.
        #
        self.loop = asyncio.get_running_loop()

    def add_event_listener(self, listener):
        self.event_queues[listener] = IterableAsyncQueue()

    def remove_event_listener(self, listener):
        if listener in self.event_queues:
            del self.event_queues[listener]

    async def notify_event(self, job_event):
        for (listener, queue) in self.event_queues.items():
            await queue.put(job_event)

    def get_event_queue(self, listener):
        if not listener in self.event_queues:
            return None
        return self.event_queues[listener]

    def set_context(self, ctx):
        self.context = ctx

    async def update_completion(self, completion_percentage):
        LOG.debug("update_completion %r", completion_percentage)
        self.job.completion_percentage = completion_percentage
        self.job.updated_at.GetCurrentTime()
        await self.notify_event(propsim_pb2.JOB_EVENT_PROGRESS)
        LOG.debug("update_completion (done) %r", completion_percentage)

    def update_completion_sync(self, completion_percentage):
        """A synchronous update_completion, callable from synchronous CPU-bound code, back into async context via the original asyncio event loop that created this job."""
        LOG.debug("update_completion_sync %r", completion_percentage)
        # Delivers all after completing predict
        asyncio.run_coroutine_threadsafe(self.update_completion(completion_percentage), self.loop)
        LOG.debug("update_completion_sync done %r", completion_percentage)

    async def run_fake(self):
        LOG.debug("job start (fake): %r, %r", self.job.id, self.params)
        loop = asyncio.get_running_loop()
        result = await loop.run_in_executor(None, time.sleep, 5)
        await self.update_completion(50.0)
        result = await loop.run_in_executor(None, time.sleep, 5)
        LOG.debug("job complete (fake): %r, %r", self.job.id, result)
        await self.update_completion(100.0)

    async def run_tirem(self):
        LOG.debug("job start (tirem): %r, %r", self.job.id, self.params)
        loop = asyncio.get_running_loop()
        try:
            os.makedirs(self._output_dir)
        except:
            LOG.exception("failed to create output_dir %r", self._output_dir)
        LOG.debug("job start tirem: %r, %r", self.job.id, self.params)
        #
        # NB: we run in a separate thread, since the concurrent.futures-like
        # futureproof subprocess monitor loop blocks.
        #
        #result = await loop.run_in_executor(
        #    None, functools.partial(predict, self.params, output_dir=self._output_dir, server_job_state=self))
        f = functools.partial(
            predict, self.params, output_dir=self._output_dir, server_job_state=self)
        await asyncio.to_thread(f)
        LOG.debug("job complete tirem: %r, %r", self.job.id, self.params)

    async def run(self):
        await self.run_tirem()
        self.job.status = propsim_pb2.JS_COMPLETE
        self.job.finished_at.GetCurrentTime()
        await self.notify_event(propsim_pb2.JOB_EVENT_STATUS)

    def add_url_data_output(self, name=None, filename=None, size=None, desc=None):
        path = self._output_dir + os.path.sep + filename
        if size is None:
            try:
                st = os.stat(path)
                size = st.st_size
            except:
                pass
        url_data = propsim_pb2.UrlData(
            url=self.config.http_file_server_urlbase + self.job.id + "/" + filename,
            size=size, desc=desc)
        job_output = propsim_pb2.JobOutput(
            id=str(uuid.uuid4()), name=name, url_data=url_data)
        self.outputs[job_output.id] = JobOutputState(job_output, path)
        self.job.outputs.append(job_output)

    def add_url_map_output(self, name=None, filename=None, size=None, desc=None):
        path = self._output_dir + os.path.sep + filename
        if size is None:
            try:
                st = os.stat(path)
                size = st.st_size
            except:
                pass
        url_data=propsim_pb2.UrlData(
            url=self.config.http_file_server_urlbase + self.job.id + "/" + filename,
            size=size, desc=desc)
        m = propsim_pb2.Map(
            id=str(uuid.uuid4()), type=propsim_pb2.MT_GEOTIFF, url_data=url_data)
        job_output = propsim_pb2.JobOutput(
            id=str(uuid.uuid4()), name=name, map=m)
        self.outputs[job_output.id] = JobOutputState(job_output, path)
        self.job.outputs.append(job_output)

    def delete(self):
        for (oid,o) in self.outputs.items():
            o.delete()
        os.rmdir(self._output_dir)
        self._output_dir = None
        self.job.status = propsim_pb2.JS_DELETED
        self.job.deleted_at.GetCurrentTime()

class JobService(propsim_grpc.JobServiceBase):
    """A class that implements the propsim.v1.propsim.JobService gRPC service."""
    def __init__(self, config):
        self.config = config
        self.service_parameters = [
            propsim_pb2.ParameterDefinition(
                name="radio_name", ptype=propsim_pb2.PARAMETER_TYPE_STRING,
                desc="Name of the simulated radio", is_required=True),
            propsim_pb2.ParameterDefinition(
                name="radio_is_tx", ptype=propsim_pb2.PARAMETER_TYPE_BOOL,
                desc="True if radio is a transmitter; else False",
                is_required=False, b=True),
            propsim_pb2.ParameterDefinition(
                name="txheight", ptype=propsim_pb2.PARAMETER_TYPE_DOUBLE,
                desc="transmitter height (m), not including building height nor elevation; simply the delta above the surface location in map",
                is_required=True),
            propsim_pb2.ParameterDefinition(
                name="rxheight", ptype=propsim_pb2.PARAMETER_TYPE_DOUBLE,
                desc="receiver height (m), not including building height nor elevation; simply the delta above the surface location in map",
                is_required=False, d=1.5),
            propsim_pb2.ParameterDefinition(
                name="radio_lon", ptype=propsim_pb2.PARAMETER_TYPE_DOUBLE,
                desc="radio longitude", is_required=True),
            propsim_pb2.ParameterDefinition(
                name="radio_lat", ptype=propsim_pb2.PARAMETER_TYPE_DOUBLE,
                desc="radio latitude", is_required=True),
            propsim_pb2.ParameterDefinition(
                name="freq", ptype=propsim_pb2.PARAMETER_TYPE_DOUBLE,
                desc="transmit frequency (Hz)",
                is_required=True),
            propsim_pb2.ParameterDefinition(
                name="polarz", ptype=propsim_pb2.PARAMETER_TYPE_STRING,
                desc="polarization of the wave produced by the tx antenna ('H' = horizontal polarization of Tx antenna and 'V' = vertical polarization)",
                is_required=False, s='H'),
            propsim_pb2.ParameterDefinition(
                name="generate_features", ptype=propsim_pb2.PARAMETER_TYPE_BOOL,
                desc="If False (default), generate only TIREM predictions; if True, also generate features (e.g. LOS)",
                is_required=False, b=False),
            propsim_pb2.ParameterDefinition(
                name="generate_features", ptype=propsim_pb2.PARAMETER_TYPE_BOOL,
                desc="If False (default), generate only TIREM predictions; if True, also generate features (e.g. LOS)",
                is_required=False, b=False),
            propsim_pb2.ParameterDefinition(
                name="gain", ptype=propsim_pb2.PARAMETER_TYPE_DOUBLE,
                desc="Antenna gain in dB",
                is_required=False, d=0.0),
            propsim_pb2.ParameterDefinition(
                name="txpower", ptype=propsim_pb2.PARAMETER_TYPE_DOUBLE,
                desc="Transmitter power (dBm).",
                is_required=False, d=0.0),
            propsim_pb2.ParameterDefinition(
                name="resolution", ptype=propsim_pb2.PARAMETER_TYPE_DOUBLE,
                desc="Resolution (side_len): grid cell side length (m)",
                is_required=False, d=1.0),
            propsim_pb2.ParameterDefinition(
                name="sampling_interval", ptype=propsim_pb2.PARAMETER_TYPE_DOUBLE,
                desc="Sampling interval along the tx-rx link. e.g. 0.5 means a given grid cell is sampled twice. If resolution of 0.5 and sampling_interval of 0.5, then the Tx-Rx horizontal array step size is 0.5 * 0.5 = 0.25 m.",
                is_required=False, d=1.0),
            propsim_pb2.ParameterDefinition(
                name="shrink_factor", ptype=propsim_pb2.PARAMETER_TYPE_INT,
                desc="Compute only the first shrink_factor columns and shrink_factor rows.  If shrink_factor is 20, only computes 1/400 of the job.  Useful only for debugging.",
                is_required=False, i=1),
        ]
        self.service_descriptor = propsim_pb2.ServiceDescriptor(
            id=config.service_id, endpoint=config.rpc_endpoint,
            description=config.service_description,
            version=version.version, api_version=version.api_version,
            persistent=False,features=[],parameters=self.service_parameters)
        self.job_id_queue = IterableAsyncQueue()
        self.jobs = dict()
        self._halt_queue = False

    async def run_job_queue(self):
        LOG.debug("running job queue")
        async for job_id in self.job_id_queue:
            LOG.debug("running queued job %r", job_id)
            job = self.jobs[job_id]
            job.job.status = propsim_pb2.JS_RUNNING
            job.job.started_at.GetCurrentTime()
            await self.notify_job_event(job_id, propsim_pb2.JOB_EVENT_STATUS)
            await job.run()

    async def enqueue_job(self, job_id):
        LOG.debug("queueing job %r", job_id)
        job = self.jobs[job_id]
        job.job.status = propsim_pb2.JS_SCHEDULED
        job.job.updated_at.GetCurrentTime()
        await self.job_id_queue.put(job_id)
        LOG.debug("notify job (queued) %r", job_id)
        await self.notify_job_event(job_id, propsim_pb2.JOB_EVENT_STATUS)
        LOG.debug("notified job (queued) %r", job_id)

    def cancel_job(self, job_id):
        pass

    async def notify_job_event(self, job_id, job_event):
        await self.jobs[job_id].notify_event(job_event)

    def make_tirem_params(self, job):
        pvd = dict()
        for pv in job.parameters:
            pvd[pv.name] = pv
        for pd in self.service_parameters:
            if pd.is_required and not pd.name in pvd:
                raise GRPCError(Status.INVALID_ARGUMENT,
                                message="missing required parameter %r" % (pd.name,))
        radio_is_tx = True
        if "radio_is_tx" in pvd:
            radio_is_tx = pvd["radio_is_tx"].b
        polarz = 'H'
        if "polarz" in pvd:
            polarz = pvd["polarz"].s
        rxheight = 1.5
        if "rxheight" in pvd:
            rxheight = pvd["rxheight"].d
        tp = TiremParams(
            pvd["radio_name"].s, radio_is_tx, pvd["txheight"].d,
            rxheight, pvd["radio_lon"].d, pvd["radio_lat"].d, pvd["freq"].d,
            polarz)
        if pvd.get("generate_features", None) is not None:
            tp.generate_features = pvd["generate_features"].b
        # Handle resolution -> side_len specially, for propsim API compat.
        if pvd.get("resolution", None) is not None:
            setattr(tp, "side_len", pvd["resolution"].d)
        for pname in ["gain", "txpower", "sampling_interval"]:
            if pvd.get(pname, None) is not None:
                setattr(tp, pname, pvd[pname].d)
        for pname in ["shrink_factor"]:
            if pvd.get(pname, None) is not None:
                setattr(tp, pname, pvd[pname].i)
        return tp

    def _validate(self, msg):
        try:
            validate_all(msg)
        except ValidationFailed as err:
            if len(err.args) > 1:
                message = err.__class__.__name__ + ": " + ", ".join(err.args)
            elif len(err.args) < 1:
                message = repr(err)
            else:
                message = err.__class__.__name__ + ": " + err.args[0]
            raise GRPCError(Status.INVALID_ARGUMENT, message=message)

    async def GetServiceDescriptor(self, stream: grpclib.server.Stream[propsim_pb2.GetServiceDescriptorRequest, propsim_pb2.GetServiceDescriptorResponse]) -> None:
        req = await stream.recv_message()
        self._validate(req)
        resp = propsim_pb2.GetServiceDescriptorResponse(desc=self.service_descriptor)
        await stream.send_message(resp)

    async def CreateJob(self, stream: 'grpclib.server.Stream[zms.propsim.v1.propsim_pb2.CreateJobRequest, zms.propsim.v1.propsim_pb2.CreateJobResponse]') -> None:
        req = await stream.recv_message()
        self._validate(req)
        LOG.debug("CreateJob(%r): %r", req.job.id, req)

        if req.job.id in self.jobs:
            raise GRPCError(Status.ALREADY_EXISTS,
                            message="job %r exists" % (req.job.id,))
        req.job.status = propsim_pb2.JS_CREATED
        req.job.created_at.GetCurrentTime()
        params = self.make_tirem_params(req.job)
        LOG.debug("CreateJob(%r) params: \n%s", req.job.id, str(params))

        self.jobs[req.job.id] = JobState(self.config, req.job, params)

        resp = propsim_pb2.CreateJobResponse(job_id=req.job.id)
        await stream.send_message(resp)

    async def EstimateJob(self, stream: 'grpclib.server.Stream[zms.propsim.v1.propsim_pb2.EstimateJobRequest, zms.propsim.v1.propsim_pb2.EstimateJobResponse]') -> None:
        raise GRPCError(Status.UNIMPLEMENTED)

    async def StartJob(self, stream: 'grpclib.server.Stream[zms.propsim.v1.propsim_pb2.StartJobRequest, zms.propsim.v1.propsim_pb2.StartJobResponse]') -> None:
        req = await stream.recv_message()
        self._validate(req)
        if not req.job_id in self.jobs:
            raise GRPCError(Status.NOT_FOUND,
                            message="job %r does not exist" % (req.job_id,))
        await self.enqueue_job(req.job_id)
        resp = propsim_pb2.StartJobResponse()
        await stream.send_message(resp)

    async def PauseJob(self, stream: 'grpclib.server.Stream[zms.propsim.v1.propsim_pb2.PauseJobRequest, zms.propsim.v1.propsim_pb2.PauseJobResponse]') -> None:
        raise GRPCError(Status.UNIMPLEMENTED)

    async def CancelJob(self, stream: 'grpclib.server.Stream[zms.propsim.v1.propsim_pb2.CancelJobRequest, zms.propsim.v1.propsim_pb2.CancelJobResponse]') -> None:
        raise GRPCError(Status.UNIMPLEMENTED)

    async def DeleteJob(self, stream: 'grpclib.server.Stream[zms.propsim.v1.propsim_pb2.DeleteJobRequest, zms.propsim.v1.propsim_pb2.DeleteJobResponse]') -> None:
        req = await stream.recv_message()
        self._validate(req)
        if not req.job_id in self.jobs:
            raise GRPCError(Status.NOT_FOUND,
                            message="job %r does not exist" % (req.job_id,))
        LOG.debug("DeleteJob(%r)", req.job_id)
        self.cancel_job(req.job_id)
        self.jobs[req.job_id].delete()
        LOG.debug("notify job (deleted) %r", req.job_id)
        await self.notify_job_event(req.job_id, propsim_pb2.JOB_EVENT_STATUS)
        del self.jobs[req.job_id]
        resp = propsim_pb2.DeleteJobResponse()
        await stream.send_message(resp)

    async def GetJobEvents(self, stream: 'grpclib.server.Stream[zms.propsim.v1.propsim_pb2.GetJobEventsRequest, zms.propsim.v1.propsim_pb2.GetJobEventsResponse]') -> None:
        """Returns an immediate JOB_EVENT_STATUS, then send future updates as they arrive, until the stream closes or job completes.  If the client calls this on a not-completed job, we will close the stream when we see a complete or failed state.  If the client calls this on a completed or failed job that is not deleted, we will close on deletion."""
        req = await stream.recv_message()
        self._validate(req)
        lid = str(uuid.uuid4())
        job = None
        if not req.job_id in self.jobs:
            raise GRPCError(Status.NOT_FOUND,
                            message="job %r does not exist" % (req.job_id,))
        job = self.jobs[req.job_id]
        initial_status = job.job.status
        job.add_event_listener(lid)
        LOG.debug("GetJobEvents(%r) -> %r", req.job_id, lid)
        resp = propsim_pb2.GetJobEventsResponse(event=propsim_pb2.JOB_EVENT_STATUS, job=job.job)
        await stream.send_message(resp)
        async for event in job.get_event_queue(lid):
            if event is None:
                return
            resp = propsim_pb2.GetJobEventsResponse(event=event,job=job.job)
            await stream.send_message(resp)
            if job.job.status == propsim_pb2.JS_DELETED:
                LOG.debug("GetJobEvents(%r) -> %r (deleted)", req.job_id, lid)
                return
            if initial_status != job.job.status and \
              (job.job.status == propsim_pb2.JS_COMPLETE
               or job.job.status == propsim_pb2.JS_FAILED):
                LOG.debug("GetJobEvents(%r) -> %r (complete/failed)", req.job_id, lid)
                return
        LOG.debug("GetJobEvents(%r) -> %r (done)", req.job_id, lid)

    async def GetJobOutput(self, stream: 'grpclib.server.Stream[zms.propsim.v1.propsim_pb2.GetJobOutputRequest, zms.propsim.v1.propsim_pb2.GetJobOutputResponse]') -> None:
        req = await stream.recv_message()
        resp = None
        job_output = None
        if not req.job_id in self.jobs:
            raise GRPCError(Status.NOT_FOUND,
                            message="job %r does not exist" % (req.job_id,))
        elif not req.output_id in self.jobs[req.job_id].outputs:
            raise GRPCError(Status.NOT_FOUND,
                            message="job output %r does not exist" % (req.output_id,))
        else:
            job_output = self.jobs[req.job_id].outputs[req.output_id].job_outputs
        resp = propsim_pb2.GetJobOutputResponse(output=job_output)
        await stream.send_message(resp)

class PropSimServer(object):
    """Initializes and runs an asyncio version of the propsim.JobService gRPC service.  Registers with the OpenZMS Identity service if configured.  Does not yet implement the hierarchical parallelization model in the propsim.Controller service."""
    def __init__(self, config):
        self.config = config
        (self.host, self.port) = config.rpc_endpoint.split(":")
        self.port = int(self.port)
        self.registered = False
        self.job_service = JobService(config)
        self.server = grpclib.server.Server([self.job_service])
        self.secret = None

        if self.config.http_file_server_local_dir:
            if not os.access(self.config.http_file_server_local_dir, os.W_OK | os.X_OK | os.R_OK):
                raise errors.ConfigError("http_file_server_local_dir %r must have rwx mode" % (self.config.http_file_server_local_dir,))
            else:
                LOG.debug("http_file_server_local_dir %r is accessible" % (self.config.http_file_server_local_dir,))

    async def _register_with_identity(self):
        LOG.debug("registering with identity service (%r)",
                  self.config.identity_rpc_endpoint)
        (host, port) = (self.config.identity_rpc_endpoint_host,
                        self.config.identity_rpc_endpoint_port)
        async with grpclib.client.Channel(host, port) as ch:
            identity_service = identity_grpc.IdentityStub(ch)
            service = identity_pb2.Service(
                id=self.config.service_id, name=self.config.service_name,
                kind="propsim", endpoint=self.config.rpc_endpoint,
                endpoint_api_uri="", description=self.config.service_description,
                version=version.version, api_version=version.api_version)
            req = identity_pb2.RegisterServiceRequest(service=service)
            resp = await identity_service.RegisterService(req)
            self.registered = True
            self.secret = resp.secret
            LOG.debug("registering with identity service (%r -> %r)",
                      self.config.identity_rpc_endpoint, self.secret)

    async def _register_with_controller(self):
        raise NotImplementedError()

    async def register(self):
        if self.registered:
            return
        if self.config.identity_rpc_endpoint:
            return await self._register_with_identity()
        elif self.config.controller_rpc_endpoint:
            return await self._register_with_controller()

    async def run(self):
        LOG.debug("starting propsim server (%r)", self.config.rpc_endpoint)
        if platform.system() != "Windows":
            ctx = grpclib.utils.graceful_exit([self.server])
        else:
            ctx = grpclib.utils.Wrapper()
        with ctx:
            await self.server.start(self.host, self.port)
            queue_task = asyncio.create_task(self.job_service.run_job_queue())
            LOG.debug("started propsim server (%r)", self.config.rpc_endpoint)
            await self.register()
            await self.server.wait_closed()
            self.job_service.stop_job_queue()
            asyncio.gather_tasks(queue_task)
            self.deregister()
        
