#!/usr/bin/env python3

import asyncio
import sys
import os
import logging

from zms_propsim_tirem.server.grpc.server import PropSimServer
from zms_propsim_tirem.server.config import Config
from zms_propsim_tirem.util.log import configure_logging

async def main():
    if os.getenv("CONFIG_DEBUG", False):
        configure_logging(debug=True)
    if not os.getenv("GRPC_DEBUG", False):
        logging.getLogger('hpack.hpack').setLevel(logging.WARN)
    config = Config()
    config.load(argv=sys.argv[1:] if len(sys.argv) > 1 else [])
    configure_logging(debug=config.debug)
    server = PropSimServer(config)
    await server.run()

if __name__ == "__main__":
    asyncio.run(main())
